<?php

namespace HotWire\Mailer\DependencyInjection;

use HotWire\DependencyInjection\Extension;

class MailerExtension extends Extension
{
    public function load()
    {
        $smtpParameters=$this->container->get('parameters')->getSmtpParameters();
        $transport = \Swift_SmtpTransport::newInstance($smtpParameters['host'], $smtpParameters['port'])
                    ->setUsername($smtpParameters['username'])
                    ->setPassword($smtpParameters['password']);
        $mailer = \Swift_Mailer::newInstance($transport);
        $this->container->register('mailer', $mailer);
    }
}
