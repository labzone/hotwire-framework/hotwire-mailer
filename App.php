<?php

namespace HotWire\Mailer;

use HotWire\Framework\AbstractApp;

class App extends AbstractApp
{
    public function getName()
    {
        return 'HotWire:Mailer';
    }
}
